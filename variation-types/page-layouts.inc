<?php
/**
 * @file layout.inc a place to store hook implementations to make layouts "live-themer enabled".
 */

// Desktop /////////////////////////////////////////////////////////////////////

/**
 * Implementation of our "psuedo hook" hook_lt_themable().
 */
function layout_desktop_lt_themable() {
  return array(
    'title' => t('Desktop Layouts'),
    'description' => t('Change page layout for desktop computers.'),
    'type' => 'layout-desktop',
    'hook' => 'page',
    '#weight' => 1,
    '#css_weight' => 0,
  );
}

/**
 * Implementation of _settings_form().
 */
function layout_desktop_settings_form() {
  $form = array();
  
  $form['dimensions'] = dimension_presets();
  unset($form['dimensions']['size']);

  return $form;
}

// Tablet //////////////////////////////////////////////////////////////////////

/**
 * Implementation of our "psuedo hook" hook_lt_themable().
 */
function layout_tablet_lt_themable() {
  return array(
    'title' => t('Tablet Layouts'),
    'description' => t('Change page layout for tablet devices.'),
    'type' => 'layout-tablet',
    'hook' => 'page',
    '#weight' => 1,
    '#css_weight' => 0,
  );
}

/**
 * Implementation of _settings_form().
 */
function layout_tablet_settings_form() {
  $form = array();
  
  $form['dimensions'] = dimension_presets();
  unset($form['dimensions']['size']);

  return $form;
}

// Mobile //////////////////////////////////////////////////////////////////////

/**
 * Implementation of our "psuedo hook" hook_lt_themable().
 */
function layout_mobile_lt_themable() {
  return array(
    'title' => t('Mobile Layouts'),
    'description' => t('Change page layout for mobile devices.'),
    'type' => 'layout-mobile',
    'hook' => 'page',
    '#weight' => 1,
    '#css_weight' => 0,
  );
}

/**
 * Implementation of _settings_form().
 */
function layout_mobile_settings_form() {
  $form = array();
  
  $form['dimensions'] = dimension_presets();
  unset($form['dimensions']['size']);

  return $form;
}